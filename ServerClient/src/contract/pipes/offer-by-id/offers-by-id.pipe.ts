import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { BadRequestException } from '@nestjs/common/exceptions';
import {
  CreateContractDto,
  RawCreateContractDto,
} from 'src/contract/dto/contract.dto';
import { Offer } from 'src/typeorm/offer.entity';
import { OffersService } from '../../../offers/offers.service';

@Injectable()
export class OffersByIdPipe implements PipeTransform {
  constructor(private offersService: OffersService) {}

  async transform(
    contract: RawCreateContractDto,
    metadata: ArgumentMetadata,
  ): Promise<CreateContractDto> {
    if (!contract.offers.length) {
      throw new BadRequestException('Offers must be defined.');
    }

    const mappedOffers = await this.getMappedOffers(contract.offers);

    if (mappedOffers.some((offer) => !offer)) {
      throw new BadRequestException('Some of provided offer IDs not found.');
    }

    const contractWithMappedOffers = {
      contract_name: contract.contract_name,
      start_date: contract.start_date,
      end_date: contract.end_date,
      offers: await this.getMappedOffers(contract.offers),
    };

    return contractWithMappedOffers;
  }

  async getMappedOffers(offers: string[]): Promise<Offer[]> {
    const newOffers = await Promise.all(
      offers.map((offer) => {
        const desiredOffer = this.offersService.findOneById(offer);
        return desiredOffer;
      }),
    );

    return newOffers;
  }
}
