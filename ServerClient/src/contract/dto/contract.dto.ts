import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { Offer } from 'src/typeorm/offer.entity';

export class CreateContractDto {
  @IsNotEmpty()
  @ApiProperty()
  contract_name: string;

  @IsNotEmpty()
  @ApiProperty()
  start_date: Date;

  @ApiProperty()
  end_date: Date;

  @ApiProperty()
  offers: Offer[];
}

export class RawCreateContractDto {
  @IsNotEmpty()
  @ApiProperty()
  contract_name: string;

  @IsNotEmpty()
  @ApiProperty()
  start_date: Date;

  @ApiProperty()
  end_date: Date;

  @ApiProperty()
  offers: string[];
}
