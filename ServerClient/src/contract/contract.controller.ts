import {
  Controller,
  UseInterceptors,
  Get,
  Param,
  Post,
  UsePipes,
  Body,
  NotFoundException,
} from '@nestjs/common';
import { ParseIntPipe, ValidationPipe } from '@nestjs/common/pipes';
import { ClassSerializerInterceptor } from '@nestjs/common/serializer';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { ApiAuth } from 'src/auth/decorators/api-auth.decorator';
import { PerformanceInterceptor } from 'src/interceptors/performance.interceptor';
import { OffersByIdPipe } from 'src/contract/pipes/offer-by-id/offers-by-id.pipe';
import { OffersService } from 'src/offers/offers.service';
import { Contract, ExceptionResponse } from 'src/typeorm/contract.entity';
import { ContractService } from './contract.service';
import { CreateContractDto } from './dto/contract.dto';
import { BadRequestException } from '@nestjs/common/exceptions/bad-request.exception';

@ApiTags('Contract')
@Controller('Contract')
export class ContractController {
  constructor(
    private readonly contractService: ContractService,
    private readonly offerService: OffersService,
  ) {}

  @ApiAuth()
  @UseInterceptors(PerformanceInterceptor, ClassSerializerInterceptor)
  @Get()
  @ApiResponse({
    status: 200,
    description: 'Returns list of all available contracts.',
    type: [Contract],
  })
  getContracts() {
    return this.contractService.getContracts();
  }

  @Get('id/:id')
  @ApiResponse({
    status: 200,
    description: 'Returns contract by provided ID.',
    type: Contract,
  })
  @ApiResponse({
    status: 404,
    description: 'Contract {id} not found.',
  })
  async findContractById(@Param('id', ParseIntPipe) id: number) {
    const contract = await this.contractService.findContractById(id);

    if (!contract) {
      throw new NotFoundException(`Contract ${id} not found.`);
    }

    return contract;
  }

  @Post('create')
  @ApiResponse({
    status: 200,
    description: 'Contract has been successfully created.',
    type: Contract,
  })
  @ApiResponse({
    status: 400,
    type: ExceptionResponse,
    description: 'Bad request.',
  })
  @UsePipes(ValidationPipe, OffersByIdPipe)
  async createContract(@Body() createContractDto: CreateContractDto) {
    return this.contractService.createContract(createContractDto);
  }
}
