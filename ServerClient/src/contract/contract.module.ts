import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthController } from 'src/auth/controllers/auth.controller';
import { AuthService } from 'src/auth/services/auth.service';
import { OffersController } from 'src/offers/offers.controller';
import { OffersService } from 'src/offers/offers.service';
import { Customer } from 'src/typeorm';
import { Contract } from 'src/typeorm/contract.entity';
import { Offer } from 'src/typeorm/offer.entity';
import { CustomersService } from 'src/users/services/customers.service';
import { ContractController } from './contract.controller';
import { ContractService } from './contract.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Contract, Customer, Offer]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: () => ({
        secret: '123456',
        signOptions: { expiresIn: '4d' },
      }),
    }),
  ],
  controllers: [ContractController, AuthController, OffersController],
  providers: [ContractService, AuthService, CustomersService, OffersService],
})
export class ContractModule {}
