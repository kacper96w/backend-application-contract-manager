import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Contract } from 'src/typeorm/contract.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ContractService {
  constructor(
    @InjectRepository(Contract)
    private readonly contractRepository: Repository<Contract>,
  ) {}

  async createContract(
    createContractDto: Partial<Contract>,
  ): Promise<Contract> {
    const newContract = this.contractRepository.create(createContractDto);
    return this.contractRepository.save(newContract);
  }

  async findContractById(contract_id: number): Promise<Contract> {
    return this.contractRepository.findOneBy({ contract_id });
  }

  async findBy(query: Partial<Contract>): Promise<Contract[]> {
    return this.contractRepository.find({ where: query });
  }

  async getContracts(): Promise<Contract[]> {
    return this.contractRepository.find();
  }
}
