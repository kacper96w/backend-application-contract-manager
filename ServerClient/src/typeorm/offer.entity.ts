import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, JoinTable, ManyToMany, PrimaryColumn } from 'typeorm';
import { Contract } from './contract.entity';
import { Customer } from './customer.entity';

@Entity()
export class Offer {
  @PrimaryColumn()
  @ApiProperty()
  offer_id: string;

  @Column()
  @ApiProperty()
  name: string;

  @Column()
  @ApiProperty()
  type: string;

  @Column()
  @ApiProperty()
  category: string;

  @ManyToMany(() => Contract, (contract) => contract.contract_id, {
    eager: true,
  })
  @JoinTable({ name: 'contract_offers' })
  contracts: Contract[];
}

export class ExceptionResponse {
  @ApiProperty({ example: 400 })
  statusCode: number;
  @ApiProperty({ example: 'Offer {id} not found.' })
  message: string;
  @ApiProperty({ example: 'Bad Request' })
  error: string;
}

export class CreateExceptionResponse {
  @ApiProperty({ example: 400 })
  statusCode: number;
  @ApiProperty({
    example: 'Offer {id} already exists in database.',
  })
  message: string;
  @ApiProperty({ example: 'Bad Request' })
  error: string;
}
