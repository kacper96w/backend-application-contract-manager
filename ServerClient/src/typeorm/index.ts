import { Contract } from './contract.entity';
import { Role, Customer } from './customer.entity';
import { Offer } from './offer.entity';

const entities = [Customer, Role, Offer, Contract];

export { Customer };
export default entities;
