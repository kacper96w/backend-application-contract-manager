import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Customer } from './customer.entity';
import { Offer } from './offer.entity';

@Entity()
export class Contract {
  @PrimaryGeneratedColumn()
  @ApiProperty()
  contract_id: number;

  @Column()
  @ApiProperty()
  contract_name: string;

  @Column()
  @ApiProperty()
  start_date: Date;

  @Column()
  @ApiProperty()
  end_date: Date;

  @ManyToMany(() => Offer, (offer) => offer.offer_id)
  @JoinTable({ name: 'contract_offers' })
  offers: Offer[];

  @ManyToMany(() => Customer, (customer) => customer.id)
  customers: Customer[];
}

export class ExceptionResponse {
  @ApiProperty({ example: 400 })
  statusCode: number;
  @ApiProperty({ example: 'Some of provided offer IDs not found.' })
  message: string;
  @ApiProperty({ example: 'Bad Request' })
  error: string;
}
