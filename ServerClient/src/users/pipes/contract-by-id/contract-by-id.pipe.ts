import {
  ArgumentMetadata,
  Injectable,
  NotFoundException,
  PipeTransform,
} from '@nestjs/common';
import { ContractService } from 'src/contract/contract.service';

@Injectable()
export class ContractByIdPipe implements PipeTransform {
  constructor(private contractService: ContractService) {}

  async transform(contract_id: number, metadata: ArgumentMetadata) {
    console.log(contract_id);
    const contract = await this.contractService.findContractById(contract_id);

    if (!contract) {
      throw new NotFoundException(`Contract ${contract_id} not found`);
    }

    return contract;
  }
}
