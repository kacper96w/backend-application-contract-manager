import { ContractByIdPipe } from './contract-by-id.pipe';

describe('ContractByIdPipePipe', () => {
  it('should be defined', () => {
    expect(new ContractByIdPipe()).toBeDefined();
  });
});
