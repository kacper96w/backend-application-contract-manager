import { CustomerByIdPipe } from './customer-by-id.pipe';

describe('UserByIdPipe', () => {
  it('should be defined', () => {
    expect(new CustomerByIdPipe()).toBeDefined();
  });
});
