import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateOfferDto {
  @IsNotEmpty()
  @ApiProperty()
  offer_id: string;

  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @ApiProperty()
  type: string;

  @ApiProperty()
  category: string;
}
