import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Offer } from 'src/typeorm/offer.entity';
import { Repository } from 'typeorm';

@Injectable()
export class OffersService {
  constructor(
    @InjectRepository(Offer)
    private offersRepository: Repository<Offer>,
  ) {}

  async findOne(customer_id: number, name: string): Promise<Offer | null> {
    return this.offersRepository.findOneBy({ name });
  }

  async findOneById(offer_id: string): Promise<Offer | null> {
    return this.offersRepository.findOneBy({ offer_id });
  }

  async getAllOffers(): Promise<Offer[]> {
    const offers = await this.offersRepository.find();
    return offers;
  }

  async findBy(query: Partial<Offer>): Promise<Offer[]> {
    return this.offersRepository.find({ where: query });
  }

  async create(data: Partial<Offer>): Promise<Offer> {
    const offer = this.offersRepository.create(data);
    await this.offersRepository.save(offer);

    return offer;
  }
}
