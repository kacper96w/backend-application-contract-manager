import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Offer } from 'src/typeorm/offer.entity';
import { CustomersModule } from '../users/customers.module';
import { OffersService } from './offers.service';
import { OffersController } from './offers.controller';
import { Customer } from 'src/typeorm/customer.entity';
import { Contract } from 'src/typeorm/contract.entity';

@Module({
  imports: [
    CustomersModule,
    TypeOrmModule.forFeature([Customer, Offer, Contract]),
  ],
  providers: [OffersService],
  controllers: [OffersController],
})
export class OffersModule {}
