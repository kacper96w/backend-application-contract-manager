import {
  Controller,
  UseInterceptors,
  Get,
  Post,
  ClassSerializerInterceptor,
  ValidationPipe,
} from '@nestjs/common';
import { Body, Param, UsePipes } from '@nestjs/common/decorators';
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import { ApiAuth } from 'src/auth/decorators/api-auth.decorator';
import { PerformanceInterceptor } from 'src/interceptors/performance.interceptor';
import {
  CreateExceptionResponse,
  ExceptionResponse,
  Offer,
} from 'src/typeorm/offer.entity';
import { CreateOfferDto } from './dto/offer.dto';
import { OffersService } from './offers.service';
import { BadRequestException } from '@nestjs/common/exceptions';

@ApiTags('Offers')
@Controller('Offers')
export class OffersController {
  constructor(private readonly offersService: OffersService) {}

  @ApiAuth()
  @UseInterceptors(PerformanceInterceptor, ClassSerializerInterceptor)
  @Get()
  @ApiResponse({
    status: 200,
    description: 'Returns all of offers.',
    type: [Offer],
  })
  getOffers() {
    return this.offersService.getAllOffers();
  }

  @Get('id/:id')
  @ApiResponse({
    status: 200,
    description: 'Returns offer by provided ID.',
    type: Offer,
  })
  @ApiResponse({
    status: 400,
    description: 'Bad request.',
    type: ExceptionResponse,
  })
  async findOfferById(@Param('id') offer_id: string) {
    const offer = await this.offersService.findOneById(offer_id);

    if (!offer) {
      throw new BadRequestException(`Offer ${offer_id} not found.`);
    }

    return this.offersService.findOneById(offer_id);
  }

  @Post('create')
  @ApiResponse({
    status: 200,
    description: 'Offer has been created successfully.',
    type: Offer,
  })
  @ApiResponse({
    status: 400,
    type: CreateExceptionResponse,
    description: 'Bad request.',
  })
  @UsePipes(ValidationPipe)
  async createOffer(@Body() createOfferDto: CreateOfferDto): Promise<Offer> {
    const allBuyers = await this.offersService.getAllOffers();
    const existingOfferById = allBuyers.find(
      (offer) => offer.offer_id === createOfferDto.offer_id,
    );

    if (existingOfferById) {
      throw new BadRequestException(
        `Offer ${createOfferDto.offer_id} already exists in database.`,
      );
    }
    return this.offersService.create(createOfferDto);
  }
}
