<h1 align="center">
  Contract Manager
</h1>

<p align="center"> This repository is Backend API part of Contract Manager written in NestJS. This project includes API for Authentication, Contract Managment, Customers Managment, Offers Managment.
<br>
<p align="center">

<p align="center">
  <sub>Created and maintained by <a href="https://www.linkedin.com/in/kacper-wo%C5%BAniak-408aa5192/">Kacper Wozniak.</a></sub>
</p>

## Table of Contents

- [Getting Started](#getting-started)
- [Prerequisites](#Prerequisites)
- [Available Scripts](#available-scripts)
- [Setup](#setup)
- [Docker Setup](#docker-setup)

---


## Getting Started

General idea was to store customers, assign offers and contracts and make a possibility to manage entities in a lot of various ways.  

We can check for instance list of customers, contracts, offers, edit them, implement some feauters based on the paramaters defined for each entry, i.e. we can send a remider that the particular contract is going to be finished (based on parameter end_date in contract entity).  
Such structure can be used in many companies, i.e. internet providers, insurance, banks, rent and many many others.  

Example: depends on the requirement it can be futher developed, i.e. renting bike company can request for assigning start_date contract for customer when he gets bike from station and end_date when he returns bike in another station.

---

## API Documentation

https://app.swaggerhub.com/apis-docs/KACPERWOZNIAK1996_1/contract-manager_api/1.0.0

---

## Prerequisites

NodeJS
https://nodejs.org/en/

Typescript
https://www.typescriptlang.org/

PostgresQL
https://www.postgresql.org/

Docker
https://www.docker.com/

---

## Available Scripts

In the project directory, you can run:

### `npm run start:dev`

Runs the app in the development & watch mode.<br>
Open [http://localhost:3000/api](http://localhost:3000/api) to view swagger API docs in browser (only available in development mode).<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>


---

## Setup

First, you need to clone the project using

```bash
git clone https://gitlab.com/kacper96w/backend-application-currencies-report.git
```
To run the application locally in development environment use command:
```bash
npm run start:dev
```
---
## Docker Setup

**If you want to run project without docker you will not need to create .env file**

If you want to use **Docker** to deploy it on production or development stage
First create a .env file copying from .env.example and add environment for below parameters only since it will be used for building container using docker-compose

```env
SERVER_PORT=3000
DB_PASSWORD=790889097
DB_USERNAME=postgres
DB_DATABASE_NAME=work_db2
DB_PORT=5430
```

After creating env file make changes in configuration in accordance with you development environment. Follow setup guide in case you missed it.
 
Now to run containers do
```bash
docker-compose build .
docker-compose up -d
```
These commands will run 2 containers for PostgresQL and Main API.

---


